/*
    mrViewer - the professional movie and flipbook playback
    Copyright (C) 2007-2022  Gonzalo Garramuño

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 * @file   mrvWMMEngine.cpp
 * @author gga
 * @date   Tue Jul 10 03:26:02 2007
 *
 * @brief  An Audio Engine using Windows' Multimedia (WMM) engine
 *
 *
 */

#ifdef _WIN32

#include <cstdio>
#include <cstdlib>
#include <cassert>

#include <iostream>


#include "gui/mrvIO.h"
#include "gui/mrvPreferences.h"
#include "audio/mrvWaveEngine.h"

#include <mmreg.h>   // for manufacturer and product IDs


#pragma warning( disable: 4244 )

namespace
{
const char* kModule = "wmm";
}


namespace mrv {

#define THROW(x) throw( exception(x) )

unsigned int     WaveEngine::_instances = 0;

/* Microsoft speaker definitions */
#define WAVE_SPEAKER_FRONT_LEFT             0x1
#define WAVE_SPEAKER_FRONT_RIGHT            0x2
#define WAVE_SPEAKER_FRONT_CENTER           0x4
#define WAVE_SPEAKER_LOW_FREQUENCY          0x8
#define WAVE_SPEAKER_BACK_LEFT              0x10
#define WAVE_SPEAKER_BACK_RIGHT             0x20
#define WAVE_SPEAKER_FRONT_LEFT_OF_CENTER   0x40
#define WAVE_SPEAKER_FRONT_RIGHT_OF_CENTER  0x80
#define WAVE_SPEAKER_BACK_CENTER            0x100
#define WAVE_SPEAKER_SIDE_LEFT              0x200
#define WAVE_SPEAKER_SIDE_RIGHT             0x400
#define WAVE_SPEAKER_TOP_CENTER             0x800
#define WAVE_SPEAKER_TOP_FRONT_LEFT         0x1000
#define WAVE_SPEAKER_TOP_FRONT_CENTER       0x2000
#define WAVE_SPEAKER_TOP_FRONT_RIGHT        0x4000
#define WAVE_SPEAKER_TOP_BACK_LEFT          0x8000
#define WAVE_SPEAKER_TOP_BACK_CENTER        0x10000
#define WAVE_SPEAKER_TOP_BACK_RIGHT         0x20000
#define WAVE_SPEAKER_RESERVED               0x80000000

static const int channel_mask[] = {
    WAVE_SPEAKER_FRONT_CENTER,
    WAVE_SPEAKER_FRONT_LEFT   | WAVE_SPEAKER_FRONT_RIGHT,
    WAVE_SPEAKER_FRONT_LEFT   | WAVE_SPEAKER_FRONT_RIGHT  | WAVE_SPEAKER_LOW_FREQUENCY,
    WAVE_SPEAKER_FRONT_LEFT   | WAVE_SPEAKER_FRONT_RIGHT  | WAVE_SPEAKER_BACK_LEFT    | WAVE_SPEAKER_BACK_RIGHT,
    WAVE_SPEAKER_FRONT_LEFT   | WAVE_SPEAKER_FRONT_CENTER | WAVE_SPEAKER_FRONT_RIGHT  | WAVE_SPEAKER_BACK_LEFT    | WAVE_SPEAKER_BACK_RIGHT,
    WAVE_SPEAKER_FRONT_LEFT   | WAVE_SPEAKER_FRONT_CENTER | WAVE_SPEAKER_FRONT_RIGHT  | WAVE_SPEAKER_BACK_LEFT    | WAVE_SPEAKER_BACK_RIGHT     | WAVE_SPEAKER_LOW_FREQUENCY,
    WAVE_SPEAKER_FRONT_LEFT   | WAVE_SPEAKER_FRONT_CENTER | WAVE_SPEAKER_FRONT_RIGHT  | WAVE_SPEAKER_SIDE_LEFT    | WAVE_SPEAKER_SIDE_RIGHT     | WAVE_SPEAKER_BACK_CENTER  | WAVE_SPEAKER_LOW_FREQUENCY,
    WAVE_SPEAKER_FRONT_LEFT   | WAVE_SPEAKER_FRONT_CENTER | WAVE_SPEAKER_FRONT_RIGHT  | WAVE_SPEAKER_SIDE_LEFT    | WAVE_SPEAKER_SIDE_RIGHT     | WAVE_SPEAKER_BACK_LEFT  | WAVE_SPEAKER_BACK_RIGHT | WAVE_SPEAKER_LOW_FREQUENCY,
    WAVE_SPEAKER_FRONT_LEFT   | WAVE_SPEAKER_FRONT_CENTER | WAVE_SPEAKER_FRONT_RIGHT  | WAVE_SPEAKER_SIDE_LEFT    | WAVE_SPEAKER_SIDE_RIGHT     | WAVE_SPEAKER_BACK_LEFT  | WAVE_SPEAKER_BACK_CENTER  | WAVE_SPEAKER_BACK_RIGHT | WAVE_SPEAKER_LOW_FREQUENCY
};


static void MMerror(char *function, MMRESULT code)
{
    char errbuf[256];
    waveOutGetErrorText(code, errbuf, 255);
    LOG_ERROR( function << " - " << errbuf );
}


WaveEngine::WaveEngine() :
    AudioEngine(),
    _sample_size(0),
    _audio_device( NULL ),
    _buffer( NULL ),
    _data( NULL ),
    _samples_per_block( 48000 ),  // 1 second of 48khz audio
    bytesPerBlock( 0 ),
    _num_buffers( 3 ),
    _idx( 0 )
{
    initialize();
}

WaveEngine::~WaveEngine()
{
    close();
    shutdown();
}

void WaveEngine::refresh_devices()
{
    _devices.clear();

    Device def( "default", _("Default Audio Device") );
    _devices.push_back( def );

    unsigned int num = waveOutGetNumDevs();

    char name[256];
    char desc[1024];

    for (unsigned i = 0; i < num; ++i )
    {
        WAVEOUTCAPS woc;
        if ( waveOutGetDevCaps(i, &woc, sizeof(woc) ) !=
             MMSYSERR_NOERROR ) continue;

        std::string channels;
        switch( woc.wChannels )
        {
        case 1:
            channels = "Mono";
        case 2:
            channels = "Stereo";
            break;
        case 6:
            channels = "5:1";
            break;
        case 8:
            channels = "7:1";
            break;
        default:
            char buf[128];
            sprintf( buf, "%d Channels", woc.wChannels );
            channels = buf;
            break;
        }

        _channels = 0;

        sprintf( name, "%d", i );

        std::string manufacturer;
        switch( woc.wMid )
        {
        case MM_GRAVIS:
            manufacturer = "Advanced Gravis Computer Technology, Ltd.";
            break;
        case MM_ANTEX:
            manufacturer = "Antex";
            break;
        case MM_APPS:
            manufacturer = "APPS";
            break;
        case MM_ARTISOFT:
            manufacturer = "Artisoft";
            break;
        case MM_AST:
            manufacturer = "AST Research, Inc.";
            break;
        case MM_ATI:
            manufacturer = "ATI Technologies, Inc.";
            break;
        case MM_AUDIOFILE:
            manufacturer = "Audio, Inc.";
            break;
        case MM_APT:  // same as MM_AUDIOPT
        case MM_AUDIOPT:
            manufacturer = "Audio Processing Technology";
            break;
        case MM_AURAVISION:
            manufacturer = "Auravision";
            break;
        case MM_AZTECH:
            manufacturer = "Aztech Labs, Inc.";
            break;
        case MM_CANOPUS:
            manufacturer = "Canopus, Co., Ltd.";
            break;
        case MM_COMPUSIC:
            manufacturer = "Compusic";
            break;
        case MM_CAT:
            manufacturer = "Computer Aided Technology, Inc.";
            break;
        case MM_COMPUTER_FRIENDS:
            manufacturer = "Computer Friends, Inc.";
            break;
        case MM_CONTROLRES:
            manufacturer = "Control Resources Corporation";
            break;
        case MM_CREATIVE:
            manufacturer = "Creative Labs, Inc.";
            break;
        case MM_DIALOGIC:
            manufacturer = "Dialogic Corporation";
            break;
        case MM_DOLBY:
            manufacturer = "Dolby Laboratories";
            break;
        case MM_DSP_GROUP:
            manufacturer = "DSP Group, Inc.";
            break;
        case MM_DSP_SOLUTIONS:
            manufacturer = "DSP Solutions, Inc.";
            break;
        case MM_ECHO:
            manufacturer = "Echo Speech Corporation";
            break;
        case MM_ESS:
            manufacturer = "ESS Technology, Inc.";
            break;
        case MM_EVEREX:
            manufacturer = "Everex Systems, Inc.";
            break;
        case MM_EXAN:
            manufacturer = "EXAN, Ltd.";
            break;
        case MM_FUJITSU:
            manufacturer = "Fujitsu, Ltd..";
            break;
        case MM_IOMAGIC:
            manufacturer = "I/O Magic Corporation";
            break;
        case MM_ICL_PS:
            manufacturer = "ICL Personal Systems";
            break;
        case MM_OLIVETTI:
            manufacturer = "Ing. C. Olivetti & C., S.p.A.";
            break;
        case MM_ICS:
            manufacturer = "Integrated Circuit Systems, Inc.";
            break;
        case MM_INTEL:
            manufacturer = "Intel Corporation";
            break;
        case MM_INTERACTIVE:
            manufacturer = "InterActive, Inc.";
            break;
        case MM_IBM:
            manufacturer = "IBM";
            break;
        case MM_ITERATEDSYS:
            manufacturer = "Iterated Systems, Inc.";
            break;
        case MM_LOGITECH:
            manufacturer = "Logitech, Inc.";
            break;
        case MM_LYRRUS:
            manufacturer = "Lyrrus, Inc.";
            break;
        case MM_MATSUSHITA:
            manufacturer = "Matsushita Electric Corporation of America";
            break;
        case MM_MEDIAVISION:
            manufacturer = "Media Vision, Inc.";
            break;
        case MM_METHEUS:
            manufacturer = "Metheus Corporation";
            break;
        case MM_MELABS:
            manufacturer = "microEngineering Labs";
            break;
        case MM_MICROSOFT:
            manufacturer = "Microsoft Corporation";
            break;
        case MM_MOSCOM:
            manufacturer = "MOSCOM Corporation";
            break;
        case MM_MOTOROLA:
            manufacturer = "Motorola, Inc.";
            break;
        case MM_NMS:
            manufacturer = "Natural MicroSystems Corporation";
            break;
        case MM_NCR:
            manufacturer = "NCR Corporation";
            break;
        case MM_NEC:
            manufacturer = "NEC Corporation";
            break;
        case MM_NEWMEDIA:
            manufacturer = "New Media Corporation";
            break;
        case MM_OKI:
            manufacturer = "OKI";
            break;
        case MM_OPTI:
            manufacturer = "OPTi, Inc.";
            break;
        case MM_ROLAND:
            manufacturer = "Roland";
            break;
        case MM_SCALACS:
            manufacturer = "SCALACS";
            break;
        case MM_EPSON:
            manufacturer = "Epson";
            break;
        case MM_SIERRA:
            manufacturer = "Sierra Semiconductor Corporation";
            break;
        case MM_SILICONSOFT:
            manufacturer = "Silicon Software";
            break;
        case MM_SONICFOUNDRY:
            manufacturer = "Sonic Foundry";
            break;
        case MM_SPEECHCOMP:
            manufacturer = "Speech Compression";
            break;
        case MM_SUPERMAC:
            manufacturer = "Supermac";
            break;
        case MM_TANDY:
            manufacturer = "Tandy";
            break;
        case MM_KORG:
            manufacturer = "Korg";
            break;
        case MM_TRUEVISION:
            manufacturer = "TrueVision";
            break;
        case MM_TURTLE_BEACH:
            manufacturer = "Turtle Beach Systems";
            break;
        case MM_VAL:
            manufacturer = "Video Associates Labs";
            break;
        case MM_VIDEOLOGIC:
            manufacturer = "VideoLogic";
            break;
        case MM_VITEC:
            manufacturer = "Visual Information Technologies";
            break;
        case MM_VOCALTEC:
            manufacturer = "VocalTec";
            break;
        case MM_VOYETRA:
            manufacturer = "Voyetra";
            break;
        case MM_WANGLABS:
            manufacturer = "Wang Labs";
            break;
        case MM_WILLOWPOND:
            manufacturer = "Willow Pond";
            break;
        case MM_WINNOV:
            manufacturer = "Winnov";
            break;
        case MM_XEBEC:
            manufacturer = "Xebec";
            break;
        case MM_YAMAHA:
            manufacturer = "Yamaha";
            break;
        default:
            char buf[64];
            sprintf( buf, "Manufacturer: %d", woc.wMid );
            manufacturer = buf;
            break;
        }

        std::string product;
        char buf[64];
        sprintf( buf, "Product: %d", woc.wPid );
        product = buf;

        sprintf( desc, "%s (%s) - %s %s",
                 woc.szPname, channels.c_str(),
                 manufacturer.c_str(), product.c_str() );

        Device dev( name, desc );
        AudioEngine::_devices.push_back( dev );
    }

}

bool WaveEngine::initialize()
{
    if ( _instances == 0 )
    {
        refresh_devices();

        if ( ! _devices.empty() ) AudioEngine::_device_idx = 0;
        else                      AudioEngine::_device_idx = WAVE_MAPPER;
    }

    ++_instances;
    return true;
}


bool WaveEngine::shutdown()
{
    --_instances;

    close();

    delete [] _buffer;
    _buffer = NULL;

    return true;
}


float WaveEngine::volume() const
{
    return _volume;
}


void WaveEngine::volume( float v )
{
    _volume = v;
}


inline WAVEHDR* WaveEngine::get_header()
{
    return &( _buffer[ _idx ] );
}

int get_bits_per_sample( const WaveEngine::AudioFormat format )
{
    switch( format )
    {
        case WaveEngine::kFloatLSB:
        case WaveEngine::kFloatMSB:
            {
                return sizeof(float) * 8;
            }
        case WaveEngine::kS16LSB:
        case WaveEngine::kS16MSB:
            {
                return sizeof(short) * 8;
            }
        case WaveEngine::kS32LSB:
        case WaveEngine::kS32MSB:
            {
                return sizeof(int32_t) * 8;
            }
        case WaveEngine::kU8:
            return sizeof(char) * 8;
    }
    return sizeof(float) * 8;
}

MMRESULT WaveEngine::reopen( const unsigned channels,
                         const unsigned freq,
                         const AudioFormat format )
{
    WAVEFORMATEX f;
    memset( &f, 0, sizeof(f) );
    f.wBitsPerSample = get_bits_per_sample( format );
    f.nChannels = channels;
    f.nSamplesPerSec = freq;
    f.nBlockAlign = (f.wBitsPerSample / 8) * channels;
    f.nAvgBytesPerSec = freq * f.nBlockAlign;

    f.wFormatTag = WAVE_FORMAT_IEEE_FLOAT;
    unsigned device = _device_idx;
    _old_device_idx = _device_idx;
    if ( device == 0 )
        device = WAVE_MAPPER; // default device
    else
        device -= 1;

    MMRESULT result =
    waveOutOpen(&_audio_device, device, (LPCWAVEFORMATEX) &f,
                0, 0, CALLBACK_NULL | WAVE_ALLOWSYNC );
    return result;
}

bool WaveEngine::open( const unsigned channels,
                       const unsigned freq,
                       const AudioFormat format )
{
    try
    {
        close();

        WAVEFORMATEXTENSIBLE w;
        memset( &w, 0, sizeof(w) );

        w.dwChannelMask = channel_mask[ channels-1 ];
        w.Format.wBitsPerSample = get_bits_per_sample( format );


        unsigned ch = channels;
        _channels = ch;
        _sample_size = 4 * ch;
        w.Format.nChannels = ch;
        w.Format.nSamplesPerSec = freq;
        w.Format.nBlockAlign = w.Format.wBitsPerSample * ch / 8;
        w.Format.nAvgBytesPerSec = freq * w.Format.nBlockAlign;

        w.Format.wFormatTag = WAVE_FORMAT_EXTENSIBLE;
        w.Samples.wValidBitsPerSample = w.Format.wBitsPerSample;
        w.Format.cbSize = sizeof(w) - sizeof(w.Format);
        switch( format )
        {
            case kFloatLSB:
                {
                    if ( channels > 1 )
                        w.SubFormat = KSDATAFORMAT_SUBTYPE_IEEE_FLOAT;
                    break;
                }
            case kS16LSB:
                {
                    w.SubFormat = KSDATAFORMAT_SUBTYPE_PCM;
                    break;
                }
            case kS32LSB:
                {
                    w.SubFormat = KSDATAFORMAT_SUBTYPE_PCM;
                    break;
                }
        }

        unsigned device = AudioEngine::_device_idx;
        AudioEngine::_old_device_idx = device;
        if ( device == 0 )
            device = WAVE_MAPPER; // default device
        else
            device -= 1;


        DBGM1( "waveOutOpen WAVE_MAPPER? " << ( device == WAVE_MAPPER ) );

        MMRESULT result;


        result =
        waveOutOpen(&_audio_device, device, (LPCWAVEFORMATEX) &w,
                    0, 0, CALLBACK_NULL | WAVE_FORMAT_DIRECT |
                    WAVE_ALLOWSYNC );

        DBGM1( "waveOutOpen WAVE_MAPPER? ok " << ( _audio_device != NULL )
               << " result ? " << (result == MMSYSERR_NOERROR) );


        if ( result != MMSYSERR_NOERROR || _audio_device == NULL )
        {
            if( result == WAVERR_BADFORMAT )
            {
                LOG_ERROR( "waveOutOpen failed WAVERR_BADFORMAT" );
            }
            else if( result == MMSYSERR_ALLOCATED )
            {
                LOG_ERROR( "waveOutOpen failed WAVERR_ALLOCATED" );
            }
            else if ( result == MMSYSERR_INVALFLAG )
            {
                LOG_ERROR( "waveOutOpen invalid flag" );
            }
            MMerror( "waveOutOpen", result );
            close();
            _enabled = false;
            return false;
        }

        WAVEOUTCAPS caps;
        result = waveOutGetDevCaps( device, &caps, sizeof(caps));
        if (result != MMSYSERR_NOERROR) {
            MMerror("waveOutGetDevCaps()", result);
            close();
            _enabled = false;
            return false;
        }

        _audio_format = format;

        // Allocate internal sound buffer
        bytesPerBlock = w.Format.nBlockAlign * _samples_per_block;
        assert( bytesPerBlock > 16 );
        size_t bytes = _num_buffers * bytesPerBlock;
        _data = new aligned16_uint8_t[ bytes ];
        memset( _data, 0, bytes );
        DBGM1( "allocated data at " << _data << " bytes " << bytes
             << " / num " << bytes / _num_buffers);

        delete [] _buffer;
        _buffer = new WAVEHDR[ _num_buffers ];

        DBGM3( "fill headers " << _num_buffers );

        // Set header memory
        uint8_t* ptr = (uint8_t*)_data;
        for ( unsigned i = 0; i < _num_buffers; ++i )
        {
            WAVEHDR& hdr = _buffer[i];
            memset( &hdr, 0, sizeof(WAVEHDR) );

            // assert( ((ptr % w.Format.nBlockAlign) == 0 );
            // assert( ((ptr % 16) == 0 );

            hdr.lpData  = (LPSTR)ptr;
            hdr.dwFlags = WHDR_DONE;

            ptr += bytesPerBlock;
        }

        DBGM1( "enabled ok" );
        // All okay, enable device
        _enabled = true;
        return true;
    }
    catch( const AudioEngine::exception& e )
    {
        close();
        _enabled = false;
        throw(e);
    }
}

void WaveEngine::wait_audio()
{
    while ( _audio_device &&
            _enabled &&
            ( ( _buffer[ _idx ].dwFlags & WHDR_DONE ) == 0 ) )
    {
        Sleep(10);
    }
}

bool WaveEngine::play( const char* orig, const size_t size )
{
    wait_audio();

    if ( !_audio_device) {
        return false;
    }
    if ( !_enabled ) {
        return true;
    }

    MMRESULT result;

    WAVEHDR* hdr = get_header();

    assert( size > 0 );
    assert( size < bytesPerBlock );
    assert( data != NULL );
    assert( hdr->lpData != NULL );

    uint8_t* data = (uint8_t*)orig;

    unsigned sample_len = (unsigned)size / _sample_size;

    
    if ( _volume < 0.99f )
    {
        data = new uint8_t[size];
        memcpy( data, orig, size );

        int i = sample_len * _channels;
        switch( _audio_format )
        {
            case kU8:
                {
                    uint8_t* d = (uint8_t*)data;
                    while ( i-- )
                    {
                        d[i] *= _volume;
                    }
                    break;
                }
            case kS16LSB:
            case kS16MSB:
                {
                    int16_t* d = (int16_t*)data;
                    while ( i-- )
                    {
                        d[i] *= _volume;
                    }
                    break;
                }
            case kS24LSB:
            case kS24MSB:
            case kS32LSB:
            case kS32MSB:
                {
                    int32_t* d = (int32_t*)data;
                    while ( i-- )
                    {
                        d[i] *= _volume;
                    }
                    break;
                }
            case kFloatLSB:
            case kFloatMSB:
                {
                    float* d = (float*)data;
                    while ( i-- )
                    {
                        d[i] *= _volume;
                    }
                    break;
                }
            default:
                break;
        }
    }

    // Copy data
    memcpy( hdr->lpData, data, size );
    
    if ( (uint8_t*)orig != data )
    {
        delete [] data;
    }

    hdr->dwBufferLength = (DWORD)size;
    hdr->dwLoops        = 1;
    hdr->dwFlags        = 0;

    _idx = ( _idx + 1 ) % _num_buffers;

    result = waveOutPrepareHeader(_audio_device, hdr, sizeof(WAVEHDR));

    if ( result != MMSYSERR_NOERROR || !(hdr->dwFlags & WHDR_PREPARED) )
    {
        _enabled = false;
        MMerror( "waveOutPrepareHeader", result);
        return false;
    }

    result = waveOutWrite(_audio_device, hdr, sizeof(WAVEHDR));

    if ( result != MMSYSERR_NOERROR )
    {
        _enabled = false;
        MMerror("waveOutWrite", result);
        return false;
    }


    return true;
}


void WaveEngine::free_headers()
{
    if (! _audio_device ) return;

    MMRESULT result;

    for ( unsigned i = 0; i < _num_buffers; ++i )
    {
        if ( _buffer[i].dwFlags & WHDR_PREPARED )
        {
            result = waveOutUnprepareHeader( _audio_device, &_buffer[i],
                                             sizeof(WAVEHDR) );
            if ( result != MMSYSERR_NOERROR )
            {
                _enabled = false;
                MMerror( "waveOutUnprepareHeader", result);
            }
            _buffer[i].dwFlags = WHDR_DONE;
        }
    }

    delete [] _data;
}

void WaveEngine::flush()
{
    if ( !_audio_device ) return;

    MMRESULT result = waveOutReset( _audio_device );
    _enabled = false;
    if ( result != MMSYSERR_NOERROR )
    {
        MMerror( "waveOutReset", result);
    }

    free_headers();
}


bool WaveEngine::close()
{
    if (!_audio_device) return false;

    flush();

    MMRESULT result = waveOutClose( _audio_device );
    if ( result != MMSYSERR_NOERROR )
    {
        MMerror( "waveOutClose", result);
    }

    _enabled = false;
    _audio_device = NULL;
    return true;
}


} // namespace mrv


#endif // _WIN32
